package com.datazoom.android;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.akamai.amp.ads.IAdsComponentListener;
import com.akamai.amp.ads.freewheel.AdSlot;
import com.akamai.amp.ads.freewheel.AmpFreewheelManager;
import com.akamai.amp.ads.freewheel.Freewheel;
import com.akamai.amp.ads.ima.AmpIMAManager;
import com.akamai.amp.ads.ima.IMA;
import com.akamai.amp.media.IPlayerEventsListener;
import com.akamai.amp.media.VideoPlayerContainer;
import com.akamai.amp.media.VideoPlayerView;
import com.akamai.amp.media.elements.MediaResource;
import com.akamai.amp.media.errors.ErrorType;
import com.akamai.amp.uimobile.generic.listeners.IMediaPlayerControllerListener;
import com.akamai.amp.uimobile.generic.media.PlayerControlsOverlay;
import com.datazoom.android.collector.basecollector.connection_manager.DZBeaconConnector;
import com.datazoom.android.collector.basecollector.event_collector.collector.DZEventCollector;
import com.datazoom.android.collector.basecollector.model.DatazoomConfig;
import com.datazoom.android.collector.basecollector.model.dz_events.Event;
import com.datazoom.android.collector.basecollector.util.DZUtils;
import com.datazoom.collector.akamai.AkamaiPlayerCollector;
import com.datazoom.collector.akamai.BuildConfig;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.File;
import java.io.IOException;

import tv.freewheel.utils.Logger;

import static com.datazoom.android.AppConstants.AD_TAGS;
import static com.datazoom.android.AppConstants.ASSET_IDS;
import static com.datazoom.android.AppConstants.LOG_FILE;
import static com.datazoom.android.AppConstants.VIDEOS;
import static com.datazoom.android.AppConstants.VIDEO_URLS;

/**
 * Demo Activity have the player view and controls
 */
public class MainActivity extends AppCompatActivity
        implements VideoPlayerContainer.VideoPlayerContainerCallback,
        IMediaPlayerControllerListener, IPlayerEventsListener {

    private static final String TAG = MainActivity.class.getCanonicalName();

    private int videoPosition = 0;
    private VideoPlayerContainer videoPlayerContainer;
    private VideoPlayerView player;
    private AmpIMAManager ampIMAManager;
    private PlayerControlsOverlay mPlayerControlsOverlay;
    private ProgressBar pbBuffering;
    private Button btnSubmit;
    private Button btnPush;
    private TextView txtVersion;
    private EditText edtBeaconUrl, edtConfigId;
    private Spinner spinner;
    private boolean mFullScreenMode;

    private AmpFreewheelManager freewheelManager;
    public static String FW_ADS_URL = "http://demo.v.fwmrm.net/";
    public static int FW_NETWORK_ID = 42015;
    // Request parameters — these may change from request to request
    // depending on how your content is arranged in the MRM system
    public static String FW_PROFILE = "42015:android_allinone_profile";
    public static String FW_SITE_SECTION_ID = "android_allinone_demo_site_section";
    public static String FW_VIDEO_ASSET_ID = "android_allinone_demo_video";

    private void setupCustomMetadata() {
        try {
            DZEventCollector.setCustomMetadata(new JSONArray(
                    "["
                            + "{\"customPlayerName\": \"Akamai Player\"},"
                            + "{\"customDomain\": \"demo.datazoom.io\"}"
                            + "]"
            ));
        } catch (JSONException e) {
            Log.e(TAG, "Error setting custom metadata", e);
        }
    }

    private void setupCustomEvent(DZEventCollector dzEventCollector) {
        Event event = new Event("SDKLoaded", new JSONArray());
        dzEventCollector.addCustomEvent(event);
        btnPush.setOnClickListener(v -> {
            Event event1 = new Event("buttonClick", new JSONArray());
            dzEventCollector.addCustomEvent(event1);
        });
    }

    private void setupDatazoom(VideoPlayerView mPlayer) {

        // Set datazoom configuration id and configuration URL
        String configId = edtConfigId.getText().toString();
        String configUrl = edtBeaconUrl.getText().toString();
        setupCustomMetadata();
        //Initialize datazoom akamai collector
        AkamaiPlayerCollector.create(mPlayer, this)
                .setConfig(new DatazoomConfig(configId, configUrl))
                .connect(new DZBeaconConnector.ConnectionListener() {
                    @Override
                    public void onSuccess(DZEventCollector dzEventCollector) {
                        setupCustomEvent(dzEventCollector);
                        if (ampIMAManager != null) {
                            ampIMAManager.addEventsListener((IAdsComponentListener)
                                    dzEventCollector);
                        }
                        if (freewheelManager != null) {
                            freewheelManager.addEventsListener((IAdsComponentListener)
                                    dzEventCollector);
                        }

                    }

                    @Override
                    public void onError(Throwable t) {
                        pbBuffering.setVisibility(View.GONE);
                        btnSubmit.setEnabled(true);
                        Log.e(TAG, "Error creating collector.", t);
                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        initAppCenter();
        initialize();

        // Getting sessionId and sessionViewId from the library
        Log.i("DZ_SESSION_VIEW_ID ", DZUtils.getSessionViewId(MainActivity.this));
        Log.i("DZ_SESSION_ID ", DZUtils.getSessionId(MainActivity.this));

        if (savedInstanceState != null) {
            mFullScreenMode = savedInstanceState.getBoolean("FULLSCREEN");
            validateFullScreenState();
        } else {
            mFullScreenMode = false;
        }

        videoPlayerContainer.addVideoPlayerContainerCallback(this);
        mPlayerControlsOverlay.addEventsListener(this);

        btnSubmit.setOnClickListener(v -> {
            btnSubmit.setVisibility(View.GONE);
            btnPush.setVisibility(View.VISIBLE);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    videoPosition = position;
                    destroyPlayer();
                    if (!AD_TAGS[position].equals("")) {
                        if (!AD_TAGS[position].equals("FREEWHEEL")) {
                            setupAmpIMAManager();
                            ampIMAManager.setAdsUrl(AD_TAGS[position]);
                        } else {
                            callFreeWheelManager();
                        }
                    }
                    videoPlayerContainer.prepareResource(VIDEO_URLS[position]);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            ArrayAdapter<String> adapter = new ArrayAdapter<>(MainActivity.this,
                    android.R.layout.simple_spinner_item, VIDEOS);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
        });
        try {
            LogMonitor.startAppendingLogsTo(new File(getFilesDir(), LOG_FILE));
        } catch (IOException e) {
            Log.e(TAG, "Cannot create log file: " + LOG_FILE);
        }
        setInlineUI();
    }

    private void callFreeWheelManager() {

        //It initializes the manager with the network ID, Ads URL, site section ID, video Asset ID and profile values.
        freewheelManager = Freewheel.create(this, FW_NETWORK_ID,
                FW_ADS_URL,
                FW_SITE_SECTION_ID,
                FW_VIDEO_ASSET_ID,
                FW_PROFILE);

        createAdSlots();
        /*This method filters the logs fired based on a level of importance, you can select either VERBOSE, DEBUG, INFO, WARN, ERROR or ASSERT. It is set to VERBOSE by default.   */
        freewheelManager.setExternalLogLevel(Logger.ERROR);

        // It sets the reference of the VideoPlayerContainer
        freewheelManager.setVideoPlayerContainer(videoPlayerContainer);
    }


    private void createAdSlots() {
        freewheelManager.addTemporalSlots(
                new AdSlot("pre1", AmpFreewheelManager.PREROLL, 0),
                new AdSlot("post1", AmpFreewheelManager.POSTROLL, 60),
                new AdSlot("mid1", AmpFreewheelManager.MIDROLL, 30),
                new AdSlot("mid2", AmpFreewheelManager.MIDROLL, 900),
                new AdSlot("mid3", AmpFreewheelManager.MIDROLL, 1800));
    }


    @Override
    public void onVideoPlayerCreated() {
        player = videoPlayerContainer.getVideoPlayer();
        player.setMuteState(true);
        setupDatazoom(player);
        player.setProgressBarControl(pbBuffering);
        mPlayerControlsOverlay.setVideoPlayerContainer(videoPlayerContainer);
        player.setLicense(AppConstants.AMP_LICENSE);
    }

    @Override
    public void onResourceReady(MediaResource mediaResource) {
        mediaResource.setTitle(VIDEOS[videoPosition]);
        mediaResource.setGuid(ASSET_IDS[videoPosition]);
        configVideoView();
        if (ampIMAManager != null) {
            ampIMAManager.setVideoPlayerView(player);
        }
        if (freewheelManager != null) {
            freewheelManager.setVideoPlayerView(player);
        }
        player.play(mediaResource);
        player.setMuteState(true);
        player.addEventsListener(this);
    }

    @Override
    public void onResourceError(ErrorType errorType, Exception e) {
        Log.e(TAG, "Resource error: " + errorType, e);
    }

    private void configVideoView() {
        setFullScreenState(mFullScreenMode);
        player.setFullScreenMode(VideoPlayerView.FULLSCREEN_MODE_KEEP_ASPECT_RATIO_FIT_SCREEN);
        player.setKeepScreenOn(true);
        player.enableDVRfeatures(true);
        player.useContentTimeline(true);
        getCurrentOrientation();
    }

    private void initialize() {
        pbBuffering = findViewById(R.id.pbBuffering);
        txtVersion = findViewById(R.id.txtVersion);
        btnSubmit = findViewById(R.id.btnSubmit);
        btnPush = findViewById(R.id.btnPush);
        btnPush.setVisibility(View.GONE);
        videoPlayerContainer = findViewById(R.id.akamaiPlayerView);
        edtBeaconUrl = findViewById(R.id.txtUrl);
        spinner = findViewById(R.id.spnr_video);
        edtConfigId = findViewById(R.id.txtConfiguration);
        mPlayerControlsOverlay = findViewById(R.id.playerControls);
        pbBuffering.setVisibility(View.GONE);
        txtVersion.setText(getString(R.string.footer,
                com.datazoom.android.BuildConfig.VERSION_NAME,
                BuildConfig.VERSION_NAME));
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().hide();
            }
            changeOrientation(true);
            MainActivity.this.getWindow().getDecorView()
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_IMMERSIVE);
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)
                    videoPlayerContainer.getLayoutParams();
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            videoPlayerContainer.setLayoutParams(params);
            if (videoPlayerContainer.getVideoPlayer() != null) {
                videoPlayerContainer.getVideoPlayer().setFullScreen(true);
            }
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            videoPlayerContainer.getVideoPlayer();
            changeOrientation(false);
            if (player != null) {
                player.setFullScreen(false);
            }
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)
                    videoPlayerContainer.getLayoutParams();
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            params.height = 450;
            videoPlayerContainer.setLayoutParams(params);
            MainActivity.this.getWindow().getDecorView()
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            if (getSupportActionBar() != null) {
                getSupportActionBar().show();
            }
        }
    }

    private void changeOrientation(boolean isLandscape) {
        int visibility = isLandscape ? View.GONE : View.VISIBLE;
        setFullScreenState(isLandscape);
        if (player != null) {
            if (player.isPlaying() || player.isAdPlaying()) {
                txtVersion.setVisibility(visibility);
                if (btnPush.getVisibility() != View.VISIBLE) {
                    btnSubmit.setVisibility(visibility);
                }
                edtBeaconUrl.setVisibility(visibility);
                spinner.setVisibility(visibility);
                edtConfigId.setVisibility(visibility);
            } else {
                txtVersion.setVisibility(View.VISIBLE);
                if (btnPush.getVisibility() != View.VISIBLE) {
                    btnSubmit.setVisibility(View.VISIBLE);
                }
                edtBeaconUrl.setVisibility(View.VISIBLE);
                spinner.setVisibility(View.VISIBLE);
                edtConfigId.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setupAmpIMAManager() {
        ampIMAManager = IMA.create(this).buildClientSideManager();
        ampIMAManager.setVideoPlayerContainer(videoPlayerContainer);
    }

    @Override
    protected void onResume() {
        if (ampIMAManager != null) {
            ampIMAManager.onResume(true);
        }
        if (player != null) {
            player.onResume();
        }
        super.onResume();
        if (freewheelManager != null) {
            freewheelManager.onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        pausePlayer();
        if (freewheelManager != null) {
            freewheelManager.onPause();
        }
    }

    private void pausePlayer() {
        if (ampIMAManager != null) {
            ampIMAManager.onPause();
        }
        if (player != null) {
            player.onPause();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        destroyPlayer();
        LogMonitor.stopMonitoring();
    }

    private void destroyPlayer() {
        if (player != null) {
            player.onDestroy();
        }
        if (ampIMAManager != null) {
            ampIMAManager.onDestroy();
        }
        if (freewheelManager != null) {
            freewheelManager.onDestroy();
        }
        ampIMAManager = null;
        freewheelManager = null;
    }

    private void initAppCenter() {
        AppCenter.start(getApplication(), "991f7753-7945-46cd-b1e3-879da45acf12",
                Analytics.class, Crashes.class);
    }


    private void getCurrentOrientation() {
        int orientation = MainActivity.this.getResources().getConfiguration().orientation;
        if (player != null) {
            if (Configuration.ORIENTATION_LANDSCAPE == orientation) {
                player.setFullScreen(true);
            } else {
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams)
                        videoPlayerContainer.getLayoutParams();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                params.height = 450;
                videoPlayerContainer.setLayoutParams(params);
                MainActivity.this.getWindow().getDecorView()
                        .setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
                player.setFullScreen(false);
            }
            if (ampIMAManager != null) {
                player.setTimelineListener(ampIMAManager);
            }
        }
    }

    //Fullscreen , Exitfullscreen Button functionality
    private void validateFullScreenState() {
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            requestWindowFeature(Window.FEATURE_ACTION_BAR);
            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN, WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle instanceState) {
        instanceState.putBoolean("FULLSCREEN", mFullScreenMode);
        super.onSaveInstanceState(instanceState);
    }

    private void setFullScreenMode(boolean fullscreen) {
        if (fullscreen == mFullScreenMode) {
            return;
        }
        mFullScreenMode = fullscreen;
        if (fullscreen) {
            player.setFullScreen(true);
            player.setFullScreenMode(VideoPlayerView.FULLSCREEN_MODE_KEEP_ASPECT_RATIO);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            player.setFullScreen(false);
            player.setFullScreenMode(VideoPlayerView.FULLSCREEN_MODE_NONE);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        setFullScreenState(mFullScreenMode);
    }

    private void setFullScreenState(boolean fullscreen) {
//        if (fullscreen) {
//            mMediaPlayerController.setCurrentFullScreenMode(MediaPlayerController.UI_MODE_FULLSCREEN);
//            btnPush.setVisibility(View.GONE);
//        } else {
//            mMediaPlayerController.setCurrentFullScreenMode(MediaPlayerController.UI_MODE_NON_FULLSCREEN);
//            btnPush.setVisibility(View.VISIBLE);
//        }
        if (fullscreen) {
            btnPush.setVisibility(View.GONE);
        } else {
            btnPush.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onButtonClicked(int click) {
        if (click == IMediaPlayerControllerListener.MEDIAPLAYERCONTROLLER_BUTTON_FULLSCREEN) {
            setFullScreenMode(!mFullScreenMode);
        }
        if (click == IMediaPlayerControllerListener.MEDIAPLAYERCONTROLLER_BUTTON_REPLAY) {
            if (!AD_TAGS[videoPosition].equals("")) {
                if (!AD_TAGS[videoPosition].equals("FREEWHEEL")) {
                    setupAmpIMAManager();
                    ampIMAManager.setAdsUrl(AD_TAGS[videoPosition]);
                } else {
                    callFreeWheelManager();
                }
            }
        }
    }

    private void setInlineUI() {
        mPlayerControlsOverlay.overrideControlsLayout(R.layout.androidsdk_mediaplayercontroller);
        setupInlinePlayerControls();
    }

    private void setupInlinePlayerControls() {
        mPlayerControlsOverlay.managePlayPause(R.id.androidsdk_playPauseCtrl,
                R.mipmap.amp_play,
                R.mipmap.amp_pause);
        mPlayerControlsOverlay.manageCurrentPosition(R.id.androidsdk_seekbarTextCtrl);
        mPlayerControlsOverlay.manageTimeRemaining(R.id.video_duration);
        mPlayerControlsOverlay.manageScrubbing(R.id.androidsdk_seekbarCtrl,
                R.id.androidsdk_seekToLiveAction);

        mPlayerControlsOverlay.manageFullScreen(R.id.androidsdk_fullscreenCtrl,
                R.mipmap.amp_non_fullscreen_btn,
                R.mipmap.amp_fullscreen_btn);
        mPlayerControlsOverlay.manageQualityLevels(R.id.androidsdk_qualityCtrl);
    }

    @Override
    public boolean onPlayerEvent(int eventType) {
        switch (eventType) {
            case IPlayerEventsListener.PLAYER_EXTENDED_EVENT_SEEKING_SUCCEDEED:
                if (freewheelManager != null) {
                    freewheelManager.getUIEventsListener().onScrubbingEnded();
                }
                break;
        }
        return false;
    }

    @Override
    public boolean onPlayerExtendedEvent(int i, int i1, int i2) {
        return false;
    }

}

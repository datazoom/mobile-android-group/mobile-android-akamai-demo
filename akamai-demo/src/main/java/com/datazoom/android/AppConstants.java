package com.datazoom.android;

final class AppConstants {
    static final String AMP_LICENSE = "tSvYpPvo2dja_IUoqL2rHZFVIQ42ARIOpo"
            + "Xu_iVtDfDlOk-G_SaFBFXwxe1rrnvR1qA__GdCnkst2r698hLo7A==";

    static final String GA_TRACKING_CODE = "UA-127776502-2";

    static final String[] AD_TAGS = {"",
            "https://pubads.g.doubleclick.net/gampad/ads?iu=/21775744923/external/simid&description_url=https%3A%2F%2Fdevelopers.google.com%2Finteractive-media-ads&tfcd=0&npa=0&sz=640x480&gdfp_req=1&output=vast&unviewed_position_start=1&env=vp&impl=s&correlator=",
            "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpost&cmsid=496&vid=short_onecue&correlator=",
            "",
            "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dskippablelinear&correlator=",
            "FREEWHEEL",
            "FREEWHEEL",
    };

    static final String[] VIDEOS = {"VOD - No Ads", "VOD - Pre-roll, Single Ad", "VOD - Pre, Mid & Post-rolls, Single Ads",
            "LIVE - No Ads", "LIVE - Pre-roll, Single Ad","VOD - FREEWHEEL","LIVE - FREEWHEEL"};

    static final String[] VIDEO_URLS = {
            "https://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/master.m3u8",
            "https://bitmovin-a.akamaihd.net/content/playhouse-vr/m3u8s/105560.m3u8",
            "https://multiplatform-f.akamaihd.net/i/multi/april11/sintel/sintel-hd_,512x288_450_b,640x360_700_b,768x432_1000_b,1024x576_1400_m,.mp4.csmil/master.m3u8",
            "https://cph-msl.akamaized.net/hls/live/2000341/test/master.m3u8",
            "https://moctobpltc-i.akamaihd.net/hls/live/571329/eight/playlist.m3u8",
            "https://bitmovin-a.akamaihd.net/content/playhouse-vr/m3u8s/105560.m3u8",
            "https://moctobpltc-i.akamaihd.net/hls/live/571329/eight/playlist.m3u8"
    };

    static final String[] ASSET_IDS = {
            "df2f7af7b7b0",
            "889be500b086",
            "6fd155983287",
            "91c3d2000341",
            "8db8c5713290",
            "889be500b086",
            "8db8c5713290"
    };

    static final String LOG_FILE = "datazoom-log.log";
}

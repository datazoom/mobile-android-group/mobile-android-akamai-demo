# mobile-android-akamai-demo
This Android project is a sample application to demonstrate the usage of [Datazoom](https://www.datazoom.io/ "Title")'s Android AkamaiPlayer collector.

Datazoom’s Akamai Collector facilitates Akamai android applications to send video playback events based on the configuration created in data-pipes.

## Akamai Integration
The application uses amp-ui-mobile library from Akamai Premier releases:

AMP for Devices - Premier : Android Build 9.0.8 (https://mdtp-a.akamaihd.net/amp-android-sdk/premier)


## Developer Instructions

### Prerequisites

Mobile android base collector requires [Java 8](https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html) to run.

Install Android Studio and Android Software Development Kit (SDK):

https://developer.android.com/studio/install

Update the IDE and SDK Tools:

https://developer.android.com/studio/intro/update

### Installation
**Clone:**

Clone the project in to the local storage

```sh
$ git clone git@gitlab.com:datazoom/mobile-android-group/mobile-android-akamai-demo.git
```

**Setup:**

This project uses the following libraries that are not fetched from maven repository:

* *amp-ui-mobile.aar*: The AMP mobile UI library from Akamai
* *akamai-collector-<version>.aar*: The android base collector library 

You should verify these libraries are available or add them under `akamai-demo/libs` folder.

**Build & Run:** 

1. Open mobile-android-akamai-demo in Android Studio
2. Build and Run the application

## QA Instructions

A demo application can be found [here](https://gitlab.com/datazoom/mobile-android-group/mobile-android-demo-apk/tree/master/akamai-demo).

You can find the latest version by looking into the version code.

Download and install the application


## Start using the application

Open the application

The application shall show two input text boxes

1. The first one is to input the url from where the configuration has to be fetched. 
2. The second one is the configuration id which has to be used

Enter the required configuration id and url and click the submit button to use the application for capturing events. (It may take a few minutes initially for the video to load from the internet. Please wait during this time)

## Usage of collector library

You can find the usage instructions from the below url:

https://datazoom.atlassian.net/wiki/spaces/EN/pages/331218984/Akamai+Player+Android

### Dependencies

**Libraries:**
* [Akamai AMP Mobile UI](http://projects.mediadev.edgesuite.net/customers/akamai/releases/android/premier)
* [Mobile Android Akamai Collector](https://gitlab.com/datazoom/mobile-android-group/mobile-android-akamai-collector)
* [Retrofit](https://square.github.io/retrofit/)
* [Google Mobile Ads](https://developers.google.com/admob/android/sdk)
* [Google Analytics for Android](https://developers.google.com/analytics/devguides/collection/android/v4/)

**Plugins:**
* [Android Application](https://google.github.io/android-gradle-dsl/current/)
* [Maven](https://docs.gradle.org/current/userguide/maven_plugin.html)

### License/Confidentiality Agreement

Refer: [Datazoom, Inc License Agreement](/LICENSE)